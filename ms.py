import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np
import sys
import pymzml
from scipy.optimize import curve_fit
import scipy.signal
import re


class MSData:
    def __init__(self,filename,label="",mz_min=25,mz_max=600,mz_bin=1):
        # open file
        run = pymzml.run.Reader(filename)
        
        self.label = label
        if label == "":
            self.label = filename
            
        filename_regex = r'(.+\/)*(\d{8})_([^_]+)_([^_]+)_(.+)\.mzml'
        m = re.match(filename_regex, filename)
        if m is None:
            print('Filename for sample labeled \'%s\' does not match special-parsing regex.'%self.label)
            self.date = '00000000'
            self.exp = 'exp00'
            self.method = ''
            self.modifiers = set()
        else:
            self.date = m.group(2)
            self.exp = m.group(3)
            self.method = m.group(4)
            self.modifiers = set(m.group(5).split('_'))
          
        # build array of m/z values; measured m/z centroids will be rounded to the nearest value in self.mz
        self.mz = np.arange(mz_min,mz_max,mz_bin)
        
        # array of timestamps
        self.t = np.zeros(run.info['spectrum_count'])
        
        # pull spectra from file into a 2d numpy array
        self.spectra = np.zeros((self.t.size,self.mz.size))
        for spec in run:
            i = spec.ID-1
            self.t[i] = spec.scan_time[0]
            if spec.scan_time[1] != 'second':
                print('warning! scan time is not recorded in seconds!')
            
            for peak in spec.peaks('centroided'): 
                idx = (np.abs(self.mz - peak[0])).argmin()
                self.spectra[i,idx] += peak[1]
    
        # build a chromatogram for convenience 
        self.chrom = np.sum(self.spectra,axis=1)
        print('Loaded file \'%s\' with name \'%s\'' % (filename, label))
        
    def get_chrom(self,mz_min=-1, mz_max=100000):
        
        mz_min_idx = np.argmin(np.abs(self.mz-mz_min))
        mz_max_idx = np.argmin(np.abs(self.mz-mz_max))
        return np.sum(self.spectra[:,mz_min_idx:mz_max_idx+1],axis=1)
        
    def plot_chrom(self,mz_min=-1, mz_max=100000):
        chrom = self.get_chrom(mz_min,mz_max)
        mz_min_idx = np.argmin(np.abs(self.mz-mz_min))
        mz_max_idx = np.argmin(np.abs(self.mz-mz_max))

        plt.figure(figsize=(10,5))
        
        plt.plot(self.t,chrom,label=self.label)
            
        plt.legend(loc='upper right')
        plt.xlabel('retention time (sec)')
        plt.ylabel('raw counts')
        
        
        plt.title(('m/z = %d:%d' % (self.mz[mz_min_idx],self.mz[mz_max_idx])))
        
        
class IntervalFinder:
    def __init__(self, run, mz_min=-1, mz_max=100000,max_segment_width=100,t_range=[],threshold_factor=10):
        self.run = run
        
        mz_min_idx = np.argmin(np.abs(run.mz-mz_min))
        mz_max_idx = np.argmin(np.abs(run.mz-mz_max))
        chrom = np.sum(run.spectra[:,mz_min_idx:mz_max_idx+1],axis=1)
        
        if t_range==[]:
            peak_idx = scipy.signal.find_peaks(chrom,prominence=(np.max(chrom)/threshold_factor))[0]
        else:
            start = np.argmin(np.abs(self.run.t - t_range[0]))
            end = np.argmin(np.abs(self.run.t - t_range[1]))
            peak_idx = scipy.signal.find_peaks(chrom[start:end],prominence=(np.max(chrom)/threshold_factor))[0]+start
            
        self.peak_idx = peak_idx
        self.num_peaks = peak_idx.size
        
        
        if self.num_peaks == 0:
            print('no peaks found!')
            return
        print(('found %d peaks at times: ' + str(self.run.t[self.peak_idx]) ) % len(self.peak_idx))
        
        
        #### get indices ("intervals") to cut out individual peaks 
        intervals = np.zeros((peak_idx.size,2),dtype=np.int32)
        
        if self.num_peaks == 1:
            intervals[0,0] = max(0,peak_idx[0]-int(max_segment_width/2))
            intervals[0,1] = min(self.run.t.size-1,peak_idx[0]+int(max_segment_width/2))
            
        else:# self.num_peaks == 2:
            
            intervals[0,0] = max(0,peak_idx[0]-int(max_segment_width/2))
            intervals[0,1] = min(int((peak_idx[1]+peak_idx[0])/2),peak_idx[0]+int(max_segment_width/2))
            
            intervals[-1,0] = max(int((peak_idx[-1]+peak_idx[-2])/2),peak_idx[-1]-int(max_segment_width/2))
            intervals[-1,1] = min(self.run.t.size-1,peak_idx[-1]+int(max_segment_width/2))
            #
            for i in range(1,peak_idx.size-1):
                left = min(int((peak_idx[i]-peak_idx[i-1])/2),int(max_segment_width/2))
                right = min(int((peak_idx[i+1]-peak_idx[i])/2),int(max_segment_width/2))
                #width = int(min((peak_idx[i+1]-peak_idx[i-1])/4,max_segment_width/2))
                intervals[i,0] = peak_idx[i] - left
                intervals[i,1] = peak_idx[i] + right

        self.intervals = intervals
        ####
        
        plt.figure(figsize=(10,5))
        plt.plot(self.run.t/60, chrom, 'k')
        
        colors=cm.rainbow(np.linspace(0,1,len(self.intervals)))

        for i in range(len(self.intervals)):
            color = colors[i]
            start = self.intervals[i,0]
            end = self.intervals[i,1]
            
            plt.plot(self.run.t[start:end]/60, chrom[start:end], c=color)
            #plt.plot(self.run.t[]/60, run.run.chrom[run.peak_idx], 'ro')
            #for j in range(len(run.peak_idx)):
            plt.text(self.run.t[int((start+end)/2)]/60, np.max(chrom[start:end]), str(i))

        plt.xlabel('retention time (min)')
        plt.ylabel('TIC')
        plt.title('m/z = %d:%d' % (run.mz[mz_min_idx],run.mz[mz_max_idx]))
        
        
            
class Peaks:
    def __init__(self,run,intervals,mz_min=-1,mz_max=100000):
        self.run = run
        
        self.intervals = intervals
        
        
        mz_min_idx = np.argmin(np.abs(self.run.mz-mz_min))
        mz_max_idx = np.argmin(np.abs(self.run.mz-mz_max))
        self.chrom = np.sum(self.run.spectra[:,mz_min_idx:mz_max_idx+1],axis=1)
        
        self.mz_min = self.run.mz[mz_min_idx]
        self.mz_max = self.run.mz[mz_max_idx]
        
        #### now cut out segments
        self.segment_t = []
        self.segment_y = []
        self.segment_baseline = []
        self.segment_y_shifted = []
        self.segment_y_fit = []
        self.segment_t_fit = []
        
        self.trapz_area = []
        self.gaussian_area = []

        for k in range(self.intervals.shape[0]):
            iv = self.intervals[k]
            self.segment_t.append(self.run.t[iv[0]:iv[1]])
            self.segment_y.append(self.chrom[iv[0]:iv[1]])
            
            slope = (self.segment_y[-1][-1]-self.segment_y[-1][0])/(self.segment_t[-1][-1]-self.segment_t[-1][0])
            offset = self.segment_y[-1][0]
            self.segment_baseline.append(offset + slope*(self.segment_t[-1]-self.segment_t[-1][0]))
            
            self.segment_y_shifted.append(self.segment_y[-1] - self.segment_baseline[-1])

            # do gaussian fit 
            fit_succeeded = False
            try:
                o = gauss_fit(self.segment_t[-1],self.segment_y_shifted[-1])
                fit_succeeded = True
            except:
                print('warning: gaussian fit failed on peak %d' % k)
            
            if fit_succeeded:
                self.segment_t_fit.append(np.linspace(self.segment_t[-1][0],self.segment_t[-1][-1],2000))
                self.segment_y_fit.append(gauss(self.segment_t_fit[-1],o[0],o[1],o[2],o[3]))
                self.gaussian_area.append(np.abs(o[1]*o[3])*np.sqrt(2*np.pi))
            else:
                self.segment_t_fit.append(np.linspace(self.segment_t[-1][0],self.segment_t[-1][-1],10))
                self.segment_y_fit.append(np.nan*np.ones(self.segment_t_fit[-1].size))
                self.gaussian_area.append(np.nan)
                
            # calculate trapz area
            x = self.segment_t[-1]
            y = self.segment_y_shifted[-1]
            self.trapz_area.append(np.sum((x[1:] - x[:-1]) * (y[1:] + y[:-1]) / 2))
            
        ####

            
def gauss(x, H, A, x0, sigma):
    return H + A * np.exp(-np.power((x - x0), 2) / (2 * np.power(sigma, 2)))

def gauss_fit(x, y):
    mean = (x[-1]+x[0])/2
    sigma = np.abs(x[-1]-x[0])/10
    popt, pcov = curve_fit(gauss, x, y, p0=[min(y), max(y)-min(y), mean, sigma])
    return popt



def linear(x, a):
    return a*x

class Calibrator:
    def __init__(self, runs,  intervals, mz_min=-1, mz_max=100000):
        self.peaks = []
        self.intervals = np.array(intervals)
        for i in range(len(runs)):
            run = runs[i]
            print('processing run %d: %s' % (i,run.label))
            self.peaks.append(Peaks(run,self.intervals,mz_min,mz_max))
            
    
        self.num_peaks = self.intervals.shape[0]
            
        self.gaussian_area = np.zeros((self.num_peaks,len(self.peaks)))
        self.trapz_area = np.zeros((self.num_peaks,len(self.peaks)))
        
        for i in range(len(self.peaks)):
            self.gaussian_area[:,i] = self.peaks[i].gaussian_area
            self.trapz_area[:,i] = self.peaks[i].trapz_area
         
        
    def plot_peaks(self,peak_index,run_indices=[]):
        plt.figure(figsize=(8,10))
        plt.subplot(211)
        
        plot_idx = range(len(self.peaks))
        if run_indices != []:
            plot_idx = run_indices
        
        colors=cm.rainbow(np.linspace(0,1,len(plot_idx)))
        
        for i in range(len(plot_idx)):
            peaks =  self.peaks[plot_idx[i]]
            color=colors[i]
            #plt.plot(run.segment_t[peak_index],run.segment_y[peak_index],color=color,label=self.run_labels[i])
            plt.plot(peaks.segment_t[peak_index]/60,peaks.segment_y_shifted[peak_index],color=color,linestyle='none',marker='o',
                     label=peaks.run.label)
            plt.plot(peaks.segment_t_fit[peak_index]/60,peaks.segment_y_fit[peak_index],color=color,linestyle='--')
            
        plt.legend(loc='upper right')
        plt.xlabel('retention time (min)')
        plt.ylabel('counts')
        plt.title('baseline shifted, m/z=%d:%d' % (self.peaks[0].mz_min,self.peaks[0].mz_max))
        
        
        plt.subplot(212)
        
        colors=cm.rainbow(np.linspace(0,1,len(plot_idx)))
        
        for i in range(len(plot_idx)):
            peaks =  self.peaks[plot_idx[i]]
            color=colors[i]
            plt.plot(peaks.segment_t[peak_index]/60,peaks.segment_y[peak_index],'o-',color=color,label=peaks.run.label)
            
        plt.legend(loc='upper right')
        plt.xlabel('retention time (min)')
        plt.ylabel('counts')
        plt.title('raw counts, m/z=%d:%d' % (self.peaks[0].mz_min,self.peaks[0].mz_max))
        
    def plot_raw(self,run_indices=[]):
        plt.figure(figsize=(8,5))
        
        plot_idx = range(len(self.peaks))
        if run_indices != []:
            plot_idx = run_indices
        
        colors=cm.rainbow(np.linspace(0,1,len(plot_idx)))
        
        for i in range(len(plot_idx)):
            peaks =  self.peaks[plot_idx[i]]
            color=colors[i]
            #plt.plot(run.segment_t[peak_index],run.segment_y[peak_index],color=color,label=self.run_labels[i])
            plt.plot(peaks.run.t/60,peaks.chrom,color=color,label=peaks.run.label)
            
        plt.legend(loc='upper right')
        plt.xlabel('retention time (min)')
        plt.ylabel('raw counts')
        
        
    # example using override_indices dict: 
    # override_indices={1:[1,2],  4:[2,3]}
    def fit_all(self,conc,run_indices=[],override_indices={}):
        self.slopes = np.zeros(self.num_peaks)
        
        if run_indices == []:
            run_indices = range(len(self.peaks))
        
        num_cols = 3
        num_rows = int(self.num_peaks/num_cols)+1
        
        
        
        plt.figure(figsize=(num_cols*3,num_rows*3))
        for i in range(self.num_peaks):
            this_run_indices = run_indices
            if i in override_indices:
                this_run_indices = override_indices[i]
            popt, pcov = curve_fit(linear, conc[this_run_indices], self.trapz_area[i,this_run_indices])
            self.slopes[i] = popt[0]
            
            plt.subplot(num_rows,num_cols,i+1)
            plt.plot(conc,self.trapz_area[i],'ok')
            plt.plot(conc[this_run_indices],self.trapz_area[i,this_run_indices],'or')
            plt.plot(np.array([conc[0],conc[-1]]),linear(np.array([conc[0],conc[-1]]),popt[0]),'--')
            plt.title('peak %d' % i)
            plt.ylabel('counts')
            plt.xlabel('conc (mg/mL)')
        
        plt.tight_layout()
        plt.suptitle('m/z=%d:%d' % (self.peaks[0].mz_min,self.peaks[0].mz_max))

        return self.slopes