import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from itertools import cycle
import fileio
import re
import scipy.optimize
from scipy.optimize import curve_fit
from scipy import sparse
from scipy.sparse.linalg import spsolve
import scipy.optimize

class Molecule:
    def __init__(self,name, start,end,mz,vals,vt):
        self.name = name
        self.start = start
        self.end = end
        self.mz = mz
        self.vals = np.array(vals) #do here to avoid redoing
        self.vt = vt
        
    def copy(self):
        c = Molecule(self.name, self.start, self.end, self.mz.copy(), self.vals.copy(), self.vt)
        return c
        
        
class AnalysisIntervals:
    def __init__(self,example_data=None,load_filename=None):
        self.example_data = example_data
        
        if load_filename:
            self.load(load_filename)
        else:
            self.molecules = {} # a map of name->molecule
            self.background_intervals = []
        
    def add_molecule(self, name, start, end, mz, vals, vt, override=False):
        if name in self.molecules:
            if override: #replace old mol with new
                self.molecules[name] = Molecule(name,start,end,mz,vals,vt)
            else: #say that we already have it there and leave it
                print("Already have a molecule with name %s" % name)
                return
        else:#otherwise just add it in normally
            self.molecules[name] = Molecule(name,start,end,mz,vals,vt)
        
        start_idx = np.argmin(np.abs(self.example_data.t-start))
        end_idx = np.argmin(np.abs(self.example_data.t-end))
        plt.plot(self.example_data.t[start_idx:end_idx],self.example_data.get_chrom()[start_idx:end_idx],label=name)
            
            
    def set_background_intervals(self, intervals):
        self.background_intervals = intervals
        
        chrom = self.example_data.get_chrom()
        plt.figure(figsize=(8,4))
        
        plt.plot(self.example_data.t,chrom,linewidth=0.3,color='k')
        
        for interval in self.background_intervals:
            start = interval[0]
            end = interval[1]
            start_idx = np.argmin(np.abs(self.example_data.t-start))
            end_idx = np.argmin(np.abs(self.example_data.t-end))
            
            plt.plot(self.example_data.t[start_idx:end_idx],chrom[start_idx:end_idx],linewidth=2,color='r')
            
    
    def save(self,filename):
        fileio.save({'molecules':self.molecules,'background_intervals':self.background_intervals},filename, can_exit=True)

    def load(self,filename):
        obj = fileio.load(filename, can_exit=True)
        self.molecules = obj['molecules']
        self.background_intervals = obj['background_intervals']
        
    def copy(self):
        c = AnalysisIntervals(self.example_data)
        c.background_intervals = self.background_intervals.copy()
        c.molecules = {}
        for mol_name in self.molecules:
            c.molecules[mol_name] = self.molecules[mol_name].copy()
        return c
        
    # note: this function assumes background has been defined
    def get_fingerprint(self,t,mz,verbose=True, subtract_background=True):
        vec = []
        
        if subtract_background: # if we're doing the process with subtracting background
            background = Background(self.background_intervals,self.example_data)
            index_t = np.argmin(np.abs(self.example_data.t-t))

            if verbose:
                print('At time t = %0.1f:' % t)

            for this_mz in mz:
                this_mz_index = np.argmin(np.abs(self.example_data.mz-this_mz))
                this_sample_counts = self.example_data.spectra[index_t,this_mz_index]
                this_bg_counts = background.bg(t)[this_mz_index]
                this_bgsub_counts = this_sample_counts - this_bg_counts
                vec.append(max(0,round(this_bgsub_counts)))
                if verbose:
                    print('  At m/z = %0.1f:' % this_mz)
                    print('    Sample counts: %d' % (this_sample_counts))
                    print('    Background counts: %d' % (this_bg_counts))
                    print('    Sample counts (bg subtracted): %d' % (this_bgsub_counts))

            vt = sum(self.example_data.spectra[index_t])-background.bg_tic(t)

            if verbose:
                print('  Total sample counts = %d' % sum(self.example_data.spectra[index_t]))
                print('  Total bg counts = %d' % background.bg_tic(t))
                print('  Total sample - bg counts = %d' % vt)

                print('\nSuggested input to AnalysisIntervals:')
                print('  vals=' + str(vec) + ', vt=%d' % vt)

                plt.plot(self.example_data.mz,self.example_data.spectra[index_t],'k',label='sample')
                plt.plot(self.example_data.mz,background.bg(t),'c',label='background')
                plt.xlabel('m/z')
                plt.ylabel('counts')
                plt.xlim(min(mz)-10,max(mz)+10)
                plt.legend(loc='upper right')
                
        else: #don't do any background shenanigans
            index_t = np.argmin(np.abs(self.example_data.t-t))

            if verbose:
                print('At time t = %0.1f:' % t)

            for this_mz in mz:
                this_mz_index = np.argmin(np.abs(self.example_data.mz-this_mz))
                this_sample_counts = self.example_data.spectra[index_t,this_mz_index]
                vec.append(max(0,round(this_sample_counts)))
                if verbose:
                    print('  At m/z = %0.1f:' % this_mz)
                    print('    Sample counts: %d' % (this_sample_counts))

            vt = sum(self.example_data.spectra[index_t])

            if verbose:
                print('  Total sample counts = %d' % sum(self.example_data.spectra[index_t]))

                print('\nSuggested input to AnalysisIntervals:')
                print('  vals=' + str(vec) + ', vt=%d' % vt)

                plt.plot(self.example_data.mz,self.example_data.spectra[index_t],'k',label='sample')
                plt.xlabel('m/z')
                plt.ylabel('counts')
                plt.xlim(min(mz)-10,max(mz)+10)
                plt.legend(loc='upper right')
                    
        return (vec, vt)
    
    def get_molecule(self, name):
        return self.molecules[name]
    
    def add_all_molecules(self, other_ai, replace=True):
        """
        Use this function to update ai files.
        Takes all molecules in other_ai (by name) and adds them to this object. 
        By default, will resolve conflicts by using the other_ai information (overwrites)
        """
        here_mz = next(iter(self.molecules.values())).mz #get an m/z to compare to
        for new_mol in other_ai.molecules.values():
            if new_mol.mz != here_mz: # call out and don't add
                print("Molecule %s does not have appropriate m/z vector." % new_mol.name)
            else: #otherwise, go throgh and check if we should add
                if new_mol.name in self.molecules: # if we already have a molecule called that
                    if replace: #and can override
                        self.molecules[new_mol.name] = new_mol
                else: #otherwise add as is
                    self.molecules[new_mol.name] = new_mol
    
class Background:
    def __init__(self, t_intervals, data):
        self.t_intervals = t_intervals # must be in ascending time order
        
        # store mz values
        self.mz = data.mz
        
        # calculate midpoints of the intervals and average spectra
        self.t_vals = np.zeros(len(self.t_intervals)) 
        self.bg_spectra = np.zeros((len(self.t_intervals),data.mz.size))
        for i in range(len(self.t_intervals)):
            self.t_vals[i] = np.average(self.t_intervals[i])
            idx1 = np.argmin(np.abs(data.t-self.t_intervals[i][0]))
            idx2 = np.argmin(np.abs(data.t-self.t_intervals[i][1]))
            self.bg_spectra[i] = np.average(data.spectra[idx1:idx2],axis=0)
            
            
        self.bg_total = np.sum(self.bg_spectra,axis=1)
    
    def bg(self, t):
        out = np.zeros(self.bg_spectra.shape[1])
        for i in range(out.size):
            out[i] = np.interp (t, self.t_vals, self.bg_spectra[:,i])
        
        return out
    
    def bg_tic(self, t):
        return np.interp(t, self.t_vals, self.bg_total)

def baseline_als(y, lam, p, niter=10):
    L = len(y)
    D = sparse.diags([1,-2,1],[0,-1,-2], shape=(L,L-2))
    w = np.ones(L)
    for i in range(niter):
        W = sparse.spdiags(w, 0, L, L)
        Z = W + lam * D.dot(D.transpose())
        z = spsolve(Z, w*y)
        w = p * (y > z) + (1-p) * (y < z)
    return z
    
def get_family_range(new_ai, fam):
    #gets the range of carbon numbers for a family that exists in the ai object
    C_range = [100,0]
    for i in range(1,100):
        if 'C%d_%s'%(i,fam) in new_ai.molecules: #only if there's something there should we adjust the range
            C_range[0] = min(C_range[0],i)
            C_range[1] = max(C_range[1],i)
    return C_range
    
def create_rough_ai(first_ai, families):
    mz_subset = [45, 57, 71, 73, 74, 85, 87, 89, 98] # subset of mz values to use for rough solve
    mz_idxs = []
    item  = first_ai.molecules[list(first_ai.molecules.keys())[0]] #get any item to pull mz from
    for i in mz_subset:
        mz_idxs.append(item.mz.index(i))
           
    new_ai = AnalysisIntervals()
    new_ai.molecules.update(get_family_features(first_ai, families, mz_subset))
        
    mols = ['early_background', 'slope_background', 'late_background']
    for mol in mols: #add these additional molecules as is
        if mol in first_ai.molecules:
            new_ai.molecules[mol] = first_ai.molecules[mol].copy()
            new_ai.molecules[mol].mz = mz_subset.copy()
            new_ai.molecules[mol].vals = first_ai.molecules[mol].vals[mz_idxs]
            if 'background' not in mol:
                new_ai.molecules[mol].start = 0
                new_ai.molecules[mol].end = 100000 #if this isn't far enough.....then fix your method
    return new_ai
            
def get_family_features(first_ai, families, mz_subset):
    mz_idxs = []
    item  = first_ai.molecules[list(first_ai.molecules.keys())[0]] #get any item to pull mz from
    for i in mz_subset:
        mz_idxs.append(item.mz.index(i))
        
    result_map = {}
    for fam in families:
        C_range = get_family_range(first_ai, fam) #get the range of carbon numbers we can work with
        new_fingerprint = np.zeros(len(mz_idxs))
        for i in range(C_range[0], C_range[1]+1):
            new_fingerprint += first_ai.molecules['C%d_%s'%(i,fam)].vals[mz_idxs]/first_ai.molecules['C%d_%s'%(i,fam)].vt
        new_fingerprint = new_fingerprint/(C_range[1] - C_range[0] + 1)
        new_vt = 1
        new_mol = Molecule('general_%s'%fam, 0, 100000, mz_subset, new_fingerprint, new_vt)
        result_map['general_%s'%fam] = new_mol
    return result_map    

def get_optimum_adjustment(intervals, peak_locs):
    orig_intervals = intervals
    intervals = np.array(intervals)
    func = lambda x: get_misalignment_with_adjustment(intervals, peak_locs, x)
    testing = True
    potential_shifts = [20, 40, 60, -20, 0] #try bigger shifts, but end with the normal one
    while testing:
        res = scipy.optimize.minimize(func, [1,0], method='Powell', bounds=[(0.90, 1.05), (-20, 20)])
        if check_overlaps(intervals*res.x[0] + res.x[1], peak_locs): #there's still overlaps, try shaking things up
            if len(potential_shifts)>0:
                intervals = np.array(orig_intervals)
                intervals += potential_shifts.pop(0)
            else:
                testing = False
        else:
            testing = False
    return res.x
    
def get_misalignment_with_adjustment(intervals, peak_locs, adjustment):
    intervals = np.array(intervals) #make new object
    intervals = intervals*adjustment[0] + adjustment[1]
    if check_overlaps(intervals, peak_locs): #things started to overlap - severe penalty   
        return get_peak_misalignment(intervals, peak_locs) * 10
    else:
        return get_peak_misalignment(intervals, peak_locs)
    
def get_peak_misalignment(intervals, peak_locs):
    total = 0
    for peak in peak_locs:
        peak_idx = np.argmin(abs(peak-np.array(intervals)))
        total += (intervals[min(peak_idx+1, len(intervals)-1)] + intervals[peak_idx])/2 - peak #distance from center of peak
    return total
    
def check_overlaps(intervals, peak_locs):
    # checks if there's two peaks in a single interval
    used_intervals = set()
    for peak in peak_locs:
        peak_idx = np.argmin(abs(peak-np.array(intervals)))
        if peak_idx in used_intervals:
            return True
        else:
            used_intervals.add(peak_idx)
    return False #no overlaps
    
class QuantPeaks:
    def __init__(self, data, ai, verbose=True, adjust_intervals=True, clean_shape=False, clean_baseline=True, avoid_IS=False):
        self.data = data
        self.ai = ai
           
        molecules = self.ai.molecules
        self.background = Background(self.ai.background_intervals,data)
        background = self.background
        
        self.peaks = np.zeros((data.t.size,len(molecules)))
        self.molecule_index_map = {}
        for mol_name in molecules: #give unique IDs for every molecule
            self.molecule_index_map[mol_name] = len(self.molecule_index_map)
        
        if adjust_intervals:
            self.adjust_intervals(avoid_IS) # adjust the intervals
            
            molecules = self.ai.molecules #remake maps since things have shifted
            self.peaks = np.zeros((data.t.size,len(molecules)))
            self.molecule_index_map = {}
            for mol_name in molecules: #give unique IDs for every molecule
                self.molecule_index_map[mol_name] = len(self.molecule_index_map)
        
        # gather all relevant molecules at this time
        molecule_time_map = [[] for i in range(data.t.size)]
        for molecule in molecules.values(): #go through every molecule
            t_idx_start = np.argmin(np.abs(data.t-molecule.start))
            t_idx_end = np.argmin(np.abs(data.t-molecule.end))
            for t_idx in range(t_idx_start, t_idx_end):
                molecule_time_map[t_idx].append(molecule.name)
        
        for index_t in range(data.t.size):
            t = data.t[index_t]
            molecule_names = molecule_time_map[index_t]

            # if we have any molecules to work on at this time step
            if len(molecule_names)>1: #there's more than background to look at
                # go ahead and deconvolute the molecules. If fail - return
                if not self.deconvolute_molecules(index_t, molecule_names):
                    return
        
        self.clean_peaks(baseline=clean_baseline,peak_shape=clean_shape) #clean thoroughly, if requested
        self.peak_areas = np.trapz(self.peaks,x=self.data.t,axis=0)
        
        # store in a dictionary too, in case it's useful later
        self.peak_areas_dict = {} 
        for mol in molecules.values():
            self.peak_areas_dict[mol.name] = round(self.peak_areas[self.molecule_index_map[mol.name]])
            
        # print peak areas
        if verbose:
            print('found peak areas')
            print('-----------------------------')
            for (key,val) in self.peak_areas_dict.items():
                print(key + ': ' + str(val))

                
    def deconvolute_molecules(self, index_t, molecule_names_original, tolerance=0.00):
        """
        Pulled out deconvolution of molecules into separate function to clean-up code.
        Tolerance is fraction of m/z that a compound should have to be kept in the calculation
        """
        molecules = self.ai.molecules
        data = self.data
        molecule_names = molecule_names_original.copy() #copy the array, since we'll be changing this
        done = False #do we need to run the process again?
        
        # get relevant mz vals from any item in the dictionary
        mz = next(iter(molecules.values())).mz
         
        while not done: #keep resolving until we don't need to remove molecules

            # build list of unit vectors in the mz space
            vectors = []
            for idx in molecule_names:
                molecule = molecules[idx]

                # make sure every other molecule shares the same mz list; if not, error out
                if molecule.mz != mz:
                    print('mz list mismatch at time %d for molecule %s'%(str(self.data.t[index_t]), molecule.label))
                    return False

                vector = np.array(molecule.vals) / molecule.vt # normalize to vt
                vectors.append(vector)

            vectors.append(np.full(vector.shape,1)) #add row for unaccounted for peaks


            # solve for weights ratio using expression y = A*b, where b is the unknown.
            #
            #   y is a column vector containing the counts (bg subtracted) at the relevant m/z 
            #     ratios at this time step
            #
            #   A is a matrix of the vectors in m/z space for each molecule; 
            #     a given vector expresses, for the given species, the fraction of the total counts 
            #     that each m/z channel accounts for
            #
            #   b is a column vector in molecule space contaning the total counts due to each species

            A = np.array(vectors).T
            y = np.zeros(len(mz))

            for i in range(y.size):
                index_mz = np.argmin(np.abs(data.mz-mz[i]))
                y[i] = data.spectra[index_t,index_mz]

            y = np.maximum(y,0) # clip negatives to zero (negatives may arise from bg sub)

            #b = np.linalg.lstsq(A, y, rcond=None)[0] 
            b = scipy.optimize.nnls(A, y)[0] # TODO - test if scipy.optimize.nnls is better - automatically removes clippers

            b = np.maximum(b,0) # get rid of negatives from results
            N_mol = b.size-1

            # rescale the total counts based on actual totals - makes ratios of mz more important than absolute numbers
            raw_total_mz = np.sum(data.spectra[index_t,:]) #sum of mz at the values we're testing for the raw data 

            molecule_total_mz = [0]*(N_mol+1) #sum of predicted mz at the values we're testing for the compounds
            for i in range(N_mol+1): # go through every molecule
                molecule_total_mz[i] = np.sum(b[i]*vectors[i])
                
            idx_to_remove = []
            for i in range(N_mol): # go through every molecule, if they make too little of m/z - remove
                if molecule_total_mz[i]/np.sum(molecule_total_mz)<tolerance:
                    idx_to_remove.append(i)
                    
            if len(idx_to_remove)==0:
                done = True
            else:
                for index in sorted(idx_to_remove, reverse=True): # remove the molecules that need to be remove in reverse
                    del molecule_names[index]
                
        
        for i in range(len(molecule_names_original)): 
            self.peaks[index_t,self.molecule_index_map[molecule_names_original[i]]] = 0 # set zeros everywhere first
        
        for i in range(len(molecule_names)): #adjust results by their contributions to the total, but exclude unknowns
            self.peaks[index_t,self.molecule_index_map[molecule_names[i]]] = \
                        molecule_total_mz[i]/np.sum(molecule_total_mz) * raw_total_mz  # adjust values based on contribution next
        return True
        
    # plots relevant peaks overlapping the given molecule, which can be identified by index or by name
    def plot_molecule(self,name=None):
        if not name:
            return
        if name not in self.ai.molecules:
            print('couldn\'t find that name')
            return
            
        molecule = self.ai.molecules[name]
        
        # find any molecules which overlap this molecule's range; we'll also plot these
        molecule_names = []
        for cmp_molecule in self.ai.molecules.values():
            # if start or end of cmp_molecule is within bounds of molecule, or vice versa
            if  (cmp_molecule.start >= molecule.start and cmp_molecule.start <= molecule.end)\
                 or (cmp_molecule.end >= molecule.start and cmp_molecule.end <= molecule.end)\
                 or (molecule.start >= cmp_molecule.start and molecule.start <= cmp_molecule.end)\
                 or (molecule.end >= cmp_molecule.start and molecule.end <= cmp_molecule.end):
                molecule_names.append(cmp_molecule.name)
                
                
        idx1 = np.argmin(np.abs(self.data.t-molecule.start))
        idx2 = np.argmin(np.abs(self.data.t-molecule.end))
        
        colors=cycle(cm.rainbow(np.linspace(0,1,len(molecule_names))))
        for mol_name in molecule_names:
            idx_mol = self.molecule_index_map[mol_name]
            color = next(colors)
            plt.plot(self.data.t[idx1:idx2],self.peaks[idx1:idx2,idx_mol],label=mol_name,c=color)
        plt.plot(self.data.t[idx1:idx2],self.data.get_chrom()[idx1:idx2],'k',label='raw data')
#         plt.plot(self.data.t[idx1:idx2],np.sum(self.peaks[idx1:idx2,:],axis=1)+self.background.bg_tic(self.data.t[idx1:idx2]),'c--',
#                  label='bg + peaks')
        plt.plot(self.data.t[idx1:idx2],np.sum(self.peaks[idx1:idx2,:],axis=1),'c--',
                         label='bg + peaks')
        plt.xlabel('time (s)')
        plt.ylabel('total counts')
        plt.legend(loc='upper right')
        
    def clean_peaks(self, baseline=True, peak_shape=True):
        """
        This cleans up the GC spectrum for each compound so that's it's more consistent.
        baseline = True fits a baseline to the compound spectrum and subtracts
        peak_shape = True finds the largest peak in the spectrum and sets everythhing but the peak to zero
        """
        for mol_name in self.ai.molecules: #don't go through the background
            if 'background' not in mol_name:
                curve_idx = self.molecule_index_map[mol_name]
                start_idx = np.argmin(np.abs(self.ai.molecules[mol_name].start - self.data.t))
                end_idx = np.argmin(np.abs(self.ai.molecules[mol_name].end - self.data.t))
                
                if end_idx - start_idx > 5: #have enough points to do anything
                    curve = self.peaks[start_idx:end_idx,curve_idx]
                    if baseline:
                        curve = curve - baseline_als(curve, 200000, 0.003) #subtract baseline with some parameter values
                        curve[curve<0] = 0 #zero negatives
                        #for baseline, peaks really should try to not get cut off, i.e. start at a flat plane
                    if peak_shape: #TODO this is far from perfect, fix it
                        peak_data = scipy.signal.find_peaks(curve,prominence=(np.max(curve)/20))
                        if peak_data[0].size>0: #any peaks?
                            opt_idx = np.argmax(peak_data[1]['prominences'])
                            for peak in range(peak_data[0].size):
                                if peak!=opt_idx: #remove all other peaks one by one
                                    curve[(peak_data[1]['left_bases'][peak]):(peak_data[1]['right_bases'][peak])] = 0
                                else: #remove everything on the left and right
                                    curve[0:(peak_data[1]['left_bases'][peak])] = 0
                                    curve[(peak_data[1]['right_bases'][peak]):] = 0
                    self.peaks[start_idx:end_idx,curve_idx] = curve #fix peaks back
        
    def adjust_using_IS(self, new_ai, data):
        IS = 'IS_MeBz'
        # We're hoping that the IS is unique enough that it won't get randomly solved for in other compounds
        tries = 3 # try expanding the interval a total of 3 times
        threshold = 1e5 # minimum peak area necessary to count as "found" the standard
#         IS_desired_t = 785
        IS_desired_t = (self.ai.molecules[IS].start + self.ai.molecules[IS].end)/2  
        # where we think internal standard should be to ajdust accordingly - middle of its interval                  
        search = True
        molecules = new_ai.molecules
        
        if IS not in molecules: #if we don't have the IS in the AI list, don't even try looking
            print('%s: internal standard not in ai object. Continuing without IS analysis.'%self.data.label)
            peak_delta = 0
            search = False
        
        while search:
            # map out the compounds to times - her doing all the molecules, could maybe do less
            molecules = new_ai.molecules # things are by reference, so not strictly necessary, but will do for safe measure
            molecule_time_map = [[] for i in range(data.t.size)]
            for molecule in molecules.values(): #go through every molecule
                t_idx_start = np.argmin(np.abs(data.t-molecule.start))
                t_idx_end = np.argmin(np.abs(data.t-molecule.end))
                for t_idx in range(t_idx_start, t_idx_end):
                    molecule_time_map[t_idx].append(molecule.name)
            
            # solve through where the IS should be
            t_idx_start = np.argmin(np.abs(data.t-new_ai.get_molecule(IS).start))
            t_idx_end = np.argmin(np.abs(data.t-new_ai.get_molecule(IS).end))
            for index_t in range(t_idx_start, t_idx_end):
                t = data.t[index_t]
                molecule_names = molecule_time_map[index_t]

                # if we have any molecules to work on at this time step
                if len(molecule_names)>1: #there's more than background to look at
                    # go ahead and deconvolute the molecules. If fail - return
                    if not self.deconvolute_molecules(index_t, molecule_names):
                        return
            
            self.clean_peaks(baseline=True, peak_shape=False)
            self.peak_areas = np.trapz(self.peaks,x=self.data.t,axis=0)
            
            IS_area = self.peak_areas[self.molecule_index_map[IS]]
            
            
            
            if IS_area>threshold:
                curve_idx = self.molecule_index_map[IS] # pull out the curve for the standard
                curve = self.peaks[t_idx_start:t_idx_end,curve_idx]
                peaks = scipy.signal.find_peaks(curve,prominence=(np.max(curve)/5))
                IS_peak_time = self.data.t[t_idx_start + peaks[0][0]]
                peak_delta = IS_desired_t - IS_peak_time #this is how much we need to shift everything
                
                search = False #we found the peak, done here
                print('%s: found internal standard at t=%d sec, shifting peaks by %d sec'%(self.data.label, IS_peak_time, peak_delta))
            else:
                tries -= 1
                new_ai.get_molecule(IS).start -= 10 #expand the search interval
                new_ai.get_molecule(IS).end += 10
                if tries == 0:
                    print('%s: did not find the internal standard after 3 tries, adjusting families without it.'%self.data.label)
                    search = False
                    peak_delta = 0 #don't shift anything
        
        ## SHIFT EVERYTHING EXCEPT FOR BACKGROUND TO ADJUST FOR INT.STD. SHIFT
        for mol_name in new_ai.molecules:
            if 'background' not in mol_name:
                new_ai.molecules[mol_name].start += peak_delta
                new_ai.molecules[mol_name].end += peak_delta
        
    def adjust_intervals(self, avoid_IS):
        families = ['alkane', 'lactone','fame', 'diacid']

        new_ai = self.ai.copy() # we're going to modify the AI to get everything done
        data = self.data
        
        ## TRY TO FIND THE INTERNAL STANDARD TO PIN SHIFTS
        if not avoid_IS:
            self.adjust_using_IS(new_ai, data)
        
        ## GET A ROUGH ESTIMATE OF WHERE COMPOUNDS ARE
        rough_ai = create_rough_ai(new_ai, families) #make a rough ai
        #solve for peaks, but don't go back to adjusting
        rough_qp = QuantPeaks(self.data, rough_ai, verbose=False,adjust_intervals=False, clean_shape=False, clean_baseline=True) 
        self.rough_qp = rough_qp
        
        ## FOR EACH FAMILY, EXPAND THE INTERVALS TO TRY TO CAPTURE THINGS
        for fam in families:
            #first we want to find the range of carbon numbers we can work with
            C_range = get_family_range(new_ai, fam)
            
            #then, expand the intervals for all compounds in the family so that they're close, but don't overlap
            intervals = [new_ai.molecules['C%d_%s'%(C_range[0],fam)].start] #keep track of intervals in one location as well 
            for i in range(C_range[0],C_range[1]):
                avg_time = (new_ai.molecules['C%d_%s'%(i,fam)].end + new_ai.molecules['C%d_%s'%(i+1,fam)].start)/2
                new_ai.molecules['C%d_%s'%(i,fam)].end = avg_time
                new_ai.molecules['C%d_%s'%(i+1,fam)].start = avg_time
                intervals.append(avg_time)
            intervals.append(new_ai.molecules['C%d_%s'%(C_range[1],fam)].end)
            
            # next, use the approximate locations of peaks to shift every compounds' intervals slightly
            peak_size_threshold = 5e5 # how big a peak needs to be to really analyzed
            time_closeness_threshold = 20 #if peaks are within this many seconds of one another, we'll take the most prominent peak
            t = rough_qp.data.t
            curve = rough_qp.peaks[:,rough_qp.molecule_index_map['general_%s'%fam]]
            peak_info = scipy.signal.find_peaks(curve,prominence=(np.max(curve)/20))
            peak_times = t[peak_info[0][peak_info[1]['prominences']>peak_size_threshold]]
            peak_sizes = peak_info[1]['prominences'][peak_info[1]['prominences']>peak_size_threshold]

            # make sure we have unique peaks in intervals
            modified_peaks = True #iteratively combine peaks to get unique ones
            while modified_peaks:
                modified_peaks = False #assume didn't change anything
                i = 0
                while i < peak_times.size - 1:
                    if abs(peak_times[i] - peak_times[i+1]) < time_closeness_threshold: #peaks are too close
                        if peak_sizes[i] > peak_sizes[i+1]: #first peak is larger
                            peak_times = np.delete(peak_times, i+1)
                            peak_sizes = np.delete(peak_sizes, i+1)
                        else: #second peak is bigger, remove the first
                            peak_times = np.delete(peak_times, i)
                            peak_sizes = np.delete(peak_sizes, i)
                        modified_peaks = True
                    i += 1
            # get adjustment necessary and modify the intervals to work appropriately
            adjustment = get_optimum_adjustment(intervals, peak_times)
            for i in range(C_range[0],C_range[1]+1):
                new_ai.molecules['C%d_%s'%(i,fam)].start = new_ai.molecules['C%d_%s'%(i,fam)].start * adjustment[0] + adjustment[1]
                new_ai.molecules['C%d_%s'%(i,fam)].end = new_ai.molecules['C%d_%s'%(i,fam)].end * adjustment[0] + adjustment[1]
        
        molecules = new_ai.molecules
        ## SOLVE FOR PEAKS USING LARGE INTERVALS
        self.peaks = np.zeros((data.t.size,len(molecules)))
        molecule_time_map = [[] for i in range(data.t.size)]
        for molecule in molecules.values(): #go through every molecule
            t_idx_start = np.argmin(np.abs(data.t-molecule.start))
            t_idx_end = np.argmin(np.abs(data.t-molecule.end))
            for t_idx in range(t_idx_start, t_idx_end):
                molecule_time_map[t_idx].append(molecule.name)
        
        for index_t in range(data.t.size):
            t = data.t[index_t]
            molecule_names = molecule_time_map[index_t]

            # if we have any molecules to work on at this time step
            if len(molecule_names)>1: #there's more than background to look at
                # go ahead and deconvolute the molecules. If fail - return
                if not self.deconvolute_molecules(index_t, molecule_names):
                    return
        
        self.clean_peaks(baseline=True,peak_shape=False)
        self.peak_areas = np.trapz(self.peaks,x=self.data.t,axis=0)
        
        ## TRY TO SHRINK THE INTERVAL AROUND THE LARGEST PEAK
        for fam in families:
            #first we want to find the range of carbon numbers we can work with
            C_range = get_family_range(new_ai, fam)
            #then, expand the intervals for all compounds in the family so that they're close, but don't overlap
            for i in range(C_range[0],C_range[1]+1):
                curve = self.peaks[:,self.molecule_index_map['C%d_%s'%(i,fam)]]
                peaks = scipy.signal.find_peaks(curve, prominence = np.max(curve)/4)
                if peaks[0].size==0: #uh, this wasn't supposed to happen? I'll just ignore...
                    pass
                else: 
                    # for many peaks, just do wide around the peak
                    idx = np.argmax(peaks[1]['prominences'])
                    new_ai.molecules['C%d_%s'%(i,fam)].start = self.data.t[peaks[0][idx]] - 30
                    new_ai.molecules['C%d_%s'%(i,fam)].end = self.data.t[peaks[0][idx]] + 30
                    
        self.ai = new_ai #change the AI here for this quantification process
        
def get_fingerprint_from_jdx(filename, desired_mz, vt=100): # an alternative method to get fingerprints - from NIST
    lines = []
    filename = filename.lower()
    try: #open the file and read all the lines
        f = open(filename, 'r')
        lines = f.readlines()
        f.close()
    except:
        print("Error opening file.")
        return None
    
    if 'jdx' in filename:
        reg_patt = '(\d+,\d+\s?)+'
        line_delim = ' '
        pair_delim = ','
    elif 'msp' in filename:
        reg_patt = '(\d+\s+\d+(;\s+)?)+'
        line_delim = '; '
        pair_delim = ' '
        
    spectrum = {}
    pattern = re.compile(reg_patt) #find the lines with data
    for line in lines:
        m = pattern.match(line)
        if m:
            pairs = line.split(line_delim)
            for pair in pairs:
                if pair_delim in pair:
                    p = pair.split(pair_delim)
                    spectrum[int(p[0])] = int(p[1]) #save spectrum to a map
    
    total = 0 #scale the spectrum to the desired vt
    for mz in spectrum:
        total += spectrum[mz]
    for mz in spectrum:
        spectrum[mz] = round(spectrum[mz]/total*vt,3)
    
    result = [0]*len(desired_mz)
    for i in range(len(desired_mz)): #populate the fingerprint
        if desired_mz[i] in spectrum:
            result[i] = spectrum[desired_mz[i]]
        else:
            result[i] = 0    
    
    print('\nSuggested input to AnalysisIntervals:')
    print('  vals=' + str(result) + ', vt=%d' % vt)
    
    return result
          
# curve for the linear fitting
def linear(x, a):
    return a*x 
    
class ConcCalculator:
    def __init__(self, load_filename=None):
        self.response_factors = {}
        self.max_conc = {}
        self.is_speculative = {}
        if load_filename:
            self.load(load_filename)
        
    def add_curves(self, quants, conc, compounds=[], run_indices=[], override_indices={}, plot_curves=True, include_zero=True):
        """
        Takes in all the concentration and response data available to make calibration curves so desired compounds.
        quants - a list of QuantPeak objects containing the areas of compounds at desired concentrations
        conc - an np.array matrix of concentration/compound combinations. conc[i,j] is the i-ith compound at the j-th concentration
        compounds - names of compounds for which concentrations are given
        run_indices - which runs in the quants list to use in analysis. If empty, uses all
        override_indices - can use specific indices for particular compounds. Is a dictionary, example: override_indices={1:[1,2],  4:[2,3]}
        include_zero - whether to include a 0 concentration, 0 area point for fitting
        """
        num_peaks = len(quants[0].peak_areas[:])
        slopes = []
        response_factor = []

        if run_indices == []:
            run_indices = range(len(quants))

        if plot_curves:
            num_cols = 3
            num_rows = int(num_peaks/num_cols)+1
            #plot all the responses
            plt.figure(figsize=(num_cols*3,num_rows*3))
            
        plot_num = 0
        for mol in quants[0].molecule_index_map:
            #skip molecules we aren't calibrating
            if mol in compounds:
                comp_id = compounds.index(mol)

                this_run_indices = run_indices
                if mol in override_indices:
                    this_run_indices = override_indices[i]
                this_conc = [conc[comp_id, idx] for idx in this_run_indices]

                areas = []
                all_areas = []
                for j in range(len(quants)):
                    all_areas.append(quants[j].peak_areas[quants[j].molecule_index_map[mol]])
                    if j in this_run_indices:
                        areas.append(quants[j].peak_areas[quants[j].molecule_index_map[mol]])

                if include_zero:
                    areas.insert(0,0)
                    this_conc.insert(0,0)
                    
                #curve_fit fails at these scales without a guess
                p_guess = (areas[0]-areas[-1])/(this_conc[0]-this_conc[-1])
                popt, pcov = curve_fit(linear, this_conc, areas, p0=[p_guess])

                #plot linear fit from lowest point
                min_idx = np.argmin(this_conc) #index of lowest concentration
                max_idx = np.argmax(this_conc) #index of largest concentration
                slopes.append((areas[min_idx]-areas[max_idx])/(this_conc[min_idx]-this_conc[max_idx]))
                
                self.response_factors[mol] = popt[0] #add the response factor
                self.max_conc[mol] = np.max(this_conc) #keep track of max concentration
                self.is_speculative[mol] = False #came from experimental results

                if plot_curves:
                    plt.subplot(num_rows,num_cols,plot_num+1)
                    plt.plot(conc[comp_id],all_areas,'ok')
                    plt.plot(this_conc,areas,'or')

                    #plot line between lowest and largest points
                    min_idx = np.argmin(conc[comp_id,:]) #index of lowest concentration
                    max_idx = np.argmax(conc[comp_id,:]) #index of largest concentration
                    plt.plot(np.array([conc[comp_id, min_idx],conc[comp_id,max_idx]]),
                             linear(np.array([conc[comp_id,min_idx],conc[comp_id,max_idx]]),popt[0]),'--')
                    
                    #plot linear fit from lowest point
                    min_idx = np.argmin(this_conc) #index of lowest concentration
                    max_idx = np.argmax(this_conc) #index of largest concentration
                    plt.plot(np.array([this_conc[min_idx],this_conc[max_idx]]),
                             [areas[min_idx], areas[min_idx] + slopes[-1]*this_conc[max_idx]],'--')

                    plt.title(mol)
                    plt.ylabel('counts')
                    plt.xlabel('conc (mg/mL)')
                    plot_num += 1

        if plot_curves:
            plt.tight_layout()

    
    def save(self,filename):
        fileio.save({'response_factors':self.response_factors,
                     'max_conc':self.max_conc, 
                     'is_speculative':self.is_speculative},
                    filename, can_exit = True)

    def load(self,filename):
        obj = fileio.load(filename, can_exit = True)
        self.response_factors = obj['response_factors']
        self.max_conc = obj['max_conc']
        self.is_speculative = obj['is_speculative']
        
    def add_speculative_response_factors(self, compounds, factors, max_conc=[]):
        """
        For adding response factors for compounds for which we don't have a quant curve
        """
        if len(compounds)!=len(factors):
            print('Error: compound and factor number mismatched!')
           
        for i in range(len(compounds)):
            self.response_factors[compounds[i]] = factors[i]
            if max_conc != []:
                self.max_conc[compounds[i]] = max_conc[i]
            else:
                self.max_conc[compounds[i]] = 10000
            self.is_speculative[compounds[i]] = True
        
    def compute_concentrations(self, quant_peaks, verbose=True):
        num_peaks = len(quant_peaks.peak_areas[:])
        
        result = Concentrations(quant_peaks.data.label)
        
        for mol_name in quant_peaks.ai.molecules:
            if mol_name in self.response_factors: #calibration for molecule exists
                result.conc[mol_name] = quant_peaks.peak_areas[quant_peaks.molecule_index_map[mol_name]]/self.response_factors[mol_name]
            
                code = 0 #result code - all good
                code += result.conc[mol_name]>self.max_conc[mol_name] #the concentration is outside calibration
                code = code<<1
                code += self.is_speculative[mol_name] #are we using speculative response factor

                result.codes[mol_name] = code
                
                if verbose:
                    print('%s: %.4f'% (mol_name, result.conc[mol_name]))
                
        return result
        
class Concentrations:
    def __init__(self,sample_name=''):
        self.name = sample_name
        self.conc = {}
        self.codes = {}
        
    def plot_family(self, family_name, c_range=[], calc_family_conc = False, c = [], exclude = [], IS_norm = 1):
        """
        Doesn't reset figures, should be able to stack several together
        """
        family_name = family_name.lower()
        
        conc_to_plot = []
        C_nums = []
        family_conc = 0
        
        for mol in self.conc:
            s = mol.split('_')
            if family_name in s:
                conc_to_plot.append(self.conc[mol]/IS_norm)
                C_nums.append(int(s[0].split('C')[1]))
                if C_nums[-1] in exclude:
                    C_nums.pop()
                    conc_to_plot.pop()
                else:
                    family_conc += self.conc[mol]/IS_norm
                
                if c_range:
                    if not (c_range[0]<=C_nums[-1]<c_range[1]):
                        conc_to_plot.pop()
                        C_nums.pop()
                        family_conc -= self.conc[mol]/IS_norm
        
        #sort to plot in order of C (lines will look bad if not
        combined = sorted(zip(C_nums, conc_to_plot), key=lambda pair: pair[0])
        C_nums = [x for x,_ in combined]
        conc_to_plot = [x for _,x in combined]
        
        if calc_family_conc:
            if len(c)>0:
                plt.plot(C_nums ,conc_to_plot,'o-', color = c, 
                             label='%s, %s %.02g mg/mL'%(self.name, family_name, family_conc))
            else:
                plt.plot(C_nums ,conc_to_plot ,'o-', label='%s, %s %.02g mg/mL'%(self.name, family_name, family_conc))
        else:
            if len(c)>0:
                plt.plot(C_nums ,conc_to_plot ,'o-', color = c, 
                         label='%s, %s'%(self.name, family_name))
            else:
                 plt.plot(C_nums ,conc_to_plot ,'o-', label='%s, %s'%(self.name, family_name))
        
        plt.xlabel('# carbons')
        plt.ylabel('%s concentration (mg/mL)' % family_name)
        return(family_conc)
    
    def print_cmpds(self,):
        for mol in self.conc:
            print('%s: %.4f'% (mol, self.conc[mol]))

        
        