import pickle

def load(filename, can_exit=False):
    while True:
        try:
            read_file = open(filename, "rb")
            out = pickle.load(read_file)
            return out
        except Exception as e:
            print("encountered a problem during file load:")
            print(e)
            if can_exit:
                return
        
    

    
def save(obj,filename, can_exit=False):   
    while True:
        try:
            write_file = open(filename, "wb")
            pickle.dump(obj, write_file)
            write_file.flush()
            break
        except Exception as e:
            print("encountered a problem during file write:")
            print(e)
            if can_exit:
                return

